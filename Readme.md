# Who Here - Webversion (deutsch)

(english version below)

## Datenschutzerklärung

Der Benutzer wird nicht von Who Here getrackt, es werden insbesondere auch keine
IP-Adressen aufgezeichnet.

Zur Durchführung des Spiels speichert Who Here serverseitig temporär 
Benutzerdaten.
Gespeichert werden die Einstellungen des Spiels, die eingegebenen Spielernamen,
die Antworten und die Liste der ausgewählten Fragen.
Die Daten werden nur zur Durchführung des Spiels benutzt
und weder weiterverarbeitet noch an dritte weitergegeben.

Auf die oben genannten Daten kann nur mit der generierten Raumkennung 
zugegriffen werden.
Ungefähr eine Minute nachdem der letzte Spieler das Spiel verlassen 
(d.h. den Tab im Browser geschlossen) hat, werden sämtliche gespeicherten Daten
gelöscht. Es werden keine Sicherungskopien der Daten angefertigt.

# Who Here - web version (english)

## Data protection statement

Who Here does not track any users. This includes, but is not limited to, not
logging any IP addresses.

To facilitate the game, the Who Here server keeps some data. The game settings,
player names, answers, and question list are kept.
This data is only used fot the game and is not processed any further or 
disclosed to third parties.

The game data mentioned above is only accessible via the generated room id.
About one minute after the last user closes their browser tab, all data is
erased by the server. No backups of that data are kept.

## Copyright (Free Software)

Copyright (C) 2020 the Who Here contributers

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

