import * as React from 'react';
import { useState } from 'react';
import { LinkDisplay } from './LinkDisplay';
import { StartPage } from './StartPage';
import { Lobby } from './Lobby';
import { GameUi } from './GameUi';
import * as Logic from '../gamelogic';
import { play } from './audio';
import { setLanguage, l } from '../localizations/localize';
import { AboutDisplay } from './AboutDisplay';

enum AppState {
    ChooseRoom,
    InRoom
}
export const App = () => {
    const [gameState, setGameState] = React.useState(Logic.getState);
    const [state, setState] = useState<AppState>(AppState.ChooseRoom);
    const [room, setRoom] = useState<string>();
    const [numPlayers, setNumPlayers] = useState<number>(0);
    const [about, setAbout] = useState<boolean>();

    React.useEffect(() => {
        Logic.startHeartbeat();
        Logic.Connection.addUpdateHandler(setGameState)
    }, []);


    const lang = gameState?.whoHere?.settings?.lang;
    if (lang === "en" || lang === "de") setLanguage(lang);

    if (about) {

        return (
            <div>
                <h1>{l.about}</h1>
                <a href="#" onClick={() => setAbout(false)}>{l.back}</a>
                <p>{l.dataProtection}<a href="https://git.hoppe.io/Hoppe/who-here-web/-/blob/main/Readme.md">{l.moreInfo}</a></p>
                <p>{l.license}<a href="https://git.hoppe.io/Hoppe/who-here-web/-/blob/main/Readme.md">{l.moreInfo}</a></p>
            </div>
        );

    } else {
        //Play "userJoin" when a user joins, play "userLeave" when a user leaves

        if (gameState.players) {
            var newNumPlayers = Object.keys(gameState.players).length

            if (numPlayers < newNumPlayers) {
                play("UserJoin.wav")
                setNumPlayers(newNumPlayers)
            } else if (numPlayers > newNumPlayers) {
                play("UserLeave.wav")
                setNumPlayers(newNumPlayers)
            }
        }

        if (state === AppState.ChooseRoom) {
            return <div>
                <StartPage onSubmit={async room => {
                    setRoom(room)
                    setState(AppState.InRoom)
                }} />
                <AboutDisplay onClick={() => setAbout(true)} />
            </div>
        }
        else if (!(gameState?.whoHere?.stage) || (gameState?.whoHere?.stage === "lobby") || Logic.getPlayer() === null) {
            // explanation for if:
            // - gamestate not or only partially available: show lobby to gain time and allow registration
            // - stage lobby: not started
            // - player null: name not entered, allow new players to enter their name when joining
            const playersObject = gameState?.players;
            const players = playersObject ? Object.keys(playersObject) : [];
            return (
                <span>
                    <Lobby
                        state={gameState}
                        onSubmit={Logic.startGame}
                        players={[...players]}
                        room={room!}
                    />
                    <AboutDisplay onClick={() => setAbout(true)} />
                </span>
            )
        }
        else {
            return (
                <span>
                    <GameUi gameState={gameState} />
                    <LinkDisplay room={room!} />
                    <AboutDisplay onClick={() => setAbout(true)} />
                </span>
            )
        }
    }
};
