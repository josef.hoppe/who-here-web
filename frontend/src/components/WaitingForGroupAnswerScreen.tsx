import * as React from 'react';
import { useAudioEffectOnShow } from './audio';
import { QuestionDisplay } from './QuestionDisplay';
import { l } from '../localizations/localize';

export function WaitingForGroupAnswerScreen(props: { question: string, ownAnswer: string }) {
    useAudioEffectOnShow("StateChangeOther.wav")
    return (
        <span>
            <QuestionDisplay hint={l.inYourOpinion} question={props.question} />
            {l.currentPlayerThinks} <span className="result-answer">{props.ownAnswer}</span>
            <div>{l.waitingForGroupAnswer}</div>
        </span>
    );
};