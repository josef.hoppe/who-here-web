import * as React from 'react';
import { LinkToThisRoom } from './LinkToThisRoom';
import { l } from '../localizations/localize';

export function LinkDisplay(props: { room: string }) {
    const link = LinkToThisRoom(props.room)
    return (
        <span className="linkDisplay">
            {l.linkToRoom + " "}
            <a href={link}>{link}</a>
            <span className="button" onClick={() => navigator.clipboard.writeText(link)}>
                <img src="../../img/copy_icon_white.png" alt={l.copyIconAltText} className="copyIcon" />
            </span>
        </span>)
}
