import * as React from 'react';
import { useState } from 'react';
import { InputField } from './InputField';
import { LinkDisplay } from './LinkDisplay';
import { useSyncedUrlField } from "./useSyncedUrlField";
import * as Logic from '../gamelogic'
import { LinkToThisRoom } from './LinkToThisRoom';
import { play } from './audio';
import { l, getAvailableLangs, getLanguage } from '../localizations/localize';
import { PlayerDisplay } from './PlayerDisplay';

export function Lobby(props: {
    onSubmit: () => void;
    players: string[];
    state: Logic.GameState;
    room: string
}) {
    const [submitted, setSubmitted] = useState<boolean>(false);
    const submitName = async (name: string) => {
        name = name.trim();
        if (name.length > 0) {
            writeToUrl(name);
            setSubmitted(true);
            await Logic.setPlayer(name);
        }
    };
    const writeToUrl = useSyncedUrlField("name", submitName);

    const [oldName, setOldName] = useState<String>();;

    let catEmpty = true;
    let typeEmpty = true;

    const state = props.state;
    function setAnswerTime(value: string) {
        const numValue = Number(value)
        if (numValue !== NaN) {
            Logic.setTimeForCorrectAnswer(numValue)
        }
    }
    let typeSettings: JSX.Element[] = [];
    let categorySettings: JSX.Element[] = [];
    let miscSettings: JSX.Element = <span></span>;

    var newName = props.room;
    if (newName !== oldName) {
        setOldName(newName)
        play("QuestionReveal.wav")
    }

    if (state?.whoHere?.settings) {
        const typeSettingsMap = state.whoHere.settings.types;
        const categorySettingsMap = state.whoHere.settings.categories;

        typeSettings = Object.keys(state.whoHere.settings.types).map(key =>
            <div key={key} className="subsettings-item">
                <label className="container">
                    <input key={key + typeSettingsMap[key]} type="checkbox" value={key} checked={typeSettingsMap[key]} onChange={e => Logic.setTypeSetting(key, e.target.checked)}></input>
                    <span className="checkmark"></span>
                    {l.questionTypes[key as Logic.QuestionType]}
                </label>
            </div>
        );
        categorySettings = Object.keys(state.whoHere.settings.categories).map(key =>
            <div key={key} className="subsettings-item">
                <label className="container">
                    <input key={key + categorySettingsMap[key]} type="checkbox" value={key} checked={categorySettingsMap[key]} onChange={e => Logic.setCategorySetting(key, e.target.checked)}></input>
                    <span className="checkmark"></span>
                    {Logic.localizeCategory(key, state.whoHere.settings.lang)}
                </label>
            </div>
        );

        const langOptions = getAvailableLangs().map(l => <option key={l} value={l}>{l}</option>);

        miscSettings = <div>
            <div className="subsettings-item"><label htmlFor="timeoutSelect">{l.timeForCorrectAnswerSetting} </label><input id="timoutSelect" type="number" onChange={e => setAnswerTime(e.target.value)} value={state.whoHere.settings.timeForCorrectAnswer} /></div>
            <div className="subsettings-item"><label htmlFor="langSelect">{l.languageSetting} </label>

                <select id="langSelect" onChange={e => Logic.setLang(e.target.value)} value={getLanguage()}>
                    {langOptions}
                </select>
            </div>
        </div>

        Object.values(categorySettingsMap).forEach((value) => {
            if(value) catEmpty = false;
        })

        Object.values(typeSettingsMap).forEach((value) => {
            if(value) typeEmpty = false;
        })

        
    }

    

    return (
        <span>
            <span className="title_lvl1">{l.title}</span>
            <span>{l.linkToRoom}</span>
            <span className="room-link-lobby">
                <p>{LinkToThisRoom(props.room)}</p>
                <span className="button" onClick={() => navigator.clipboard.writeText(LinkToThisRoom(props.room))}>
                    <img src="../../img/copy_icon_white.png" alt={l.copyIconAltText} className="copyIconLobby" />
                </span>

            </span>
            {submitted ? null :
                <span>
                    <span className="infotext">{l.enterNameToJoin}</span>
                    <InputField className="inputFieldCompact" onSubmit={submitName} submitButtonText={l.join} />
                </span>
            }
            <PlayerDisplay players={props.players} />
            <span>
                <span className="title_lvl2">{l.settingsTitle}</span>
                <div className="settings">
                    <div className="subsettings">
                        <div className="subsettings-title">{l.settingsMiscTitle}</div>
                        {miscSettings}
                    </div>
                    <div className="subsettings">
                        <div className="subsettings-title">{l.settingsQuestionTypeTitle}</div>
                        {typeSettings}
                    </div>
                    <div className="subsettings">
                        <div className="subsettings-title">{l.settingsQuestionCategoryTitle}</div>
                        {categorySettings}
                    </div>
                </div>
            </span>
            {(submitted && !typeEmpty && !catEmpty) ? <input type="submit" className="startButton" value={l.start} onClick={e => ((submitted && !typeEmpty && !catEmpty) ? props.onSubmit() : undefined)} /> : undefined}
            
            {typeEmpty ? <div><text>{l.noTypes}</text></div> : undefined}
            {catEmpty ? <div><text>{l.noCats}</text></div> : undefined}
        </span>
    );
};
