import * as React from 'react';
import { l } from '../localizations/localize';
import { Consideration, MapObject, Player } from '../gamelogic';

export function PlayerDisplay(props: {
    players: string[];
    size?: "small" | "large";
    useConsiderations?: boolean;
    considerations?: MapObject<Consideration>;
    considerationChanged?: (player: string, consideration: Consideration) => void;
    }) {

    const small = props.size === "small";
    const callBack = props.considerationChanged || ((p, c) => {});

    function getConsClass(player: string) : string {
        let cons = ''
        if(props?.considerations && props.considerations[player]) {
            cons = props.considerations[player];
        }
        return cons;
    }

    // displaying considerations if supplied doesn't cause unexpected behaviour because it only changes appearance when props.considerations is supplied.
    // It is used for when other players are discussing the answer to the current player's question
    const defaultPlayerMap = (player: string) => 
        <li className={small ? "playernamesmall" : "playername"} key={player}><span className={`shadow ${getConsClass(player)}`}>{player}</span></li>;
    const considerationPlayerMap = (player: string) => <li className={small ? "playernamesmall" : "playername"} key={player}>
            <span className="parentHover"><a className="cons-link" href="#" onClick={() => {callBack(player, Consideration.NEGATIVE); return false}}>❌</a></span> 
            <span className={`shadow ${getConsClass(player)}`}>{player}</span> 
            <span className="parentHover"><a className="cons-link" href="#" onClick={() => {callBack(player, Consideration.POSITIVE); return false}}>✔️</a></span>
        </li>

    const playerItems = props.players.map(props.useConsiderations ? considerationPlayerMap : defaultPlayerMap);

    return (
        <span>
        <span className={small ? "title_lvl3" : "title_lvl2"}>{l.playerListTitle}</span>
        <ul className="playerlist">
            {playerItems}
        </ul>
    </span>)
}
