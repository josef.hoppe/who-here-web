import * as React from 'react';
import { GameState } from '../gamelogic';
import * as Logic from '../gamelogic';
import { AnswerQuestionScreen } from './AnswerQuestionScreen';
import { QuestionResultScreen } from './QuestionResultScreen';
import { WaitingForGroupAnswerScreen } from './WaitingForGroupAnswerScreen';
import { WaitingForCorrectAnswerScreen } from './WaitingForCorrectAnswerScreen';
import { l } from '../localizations/localize';
import { PlayerDisplay } from './PlayerDisplay';

export function GameUi(props: { gameState: GameState }) {
    if (props.gameState?.whoHere)
        return <InternalGameUi gameState={props.gameState} />
    else
        return <span>Loading...</span>
}

function InternalGameUi(props: { gameState: GameState }) {
    const questionIndex = props.gameState.whoHere.questionState.questionIndex
    const question = props.gameState.whoHere.questions[questionIndex];
    const correctAnswer = props.gameState.whoHere.questionState.correctAnswer;
    const groupAnswer = props.gameState.whoHere.questionState.groupAnswer;

    const players = Object.keys(props.gameState.players);
    const currentPlayer = props.gameState.whoHere.currentPlayer
    if (!currentPlayer)
        return <span>Error: player is undefined or null. This should not happen - you may have to open a new room.</span>

    if (correctAnswer == null) {
        if (currentPlayer === Logic.getPlayer()) {
            return (
                <span>
                    <AnswerQuestionScreen
                        opinionHint={l.inYourOpinion}
                        onSubmit={Logic.sendCorrectAnswer}
                        question={question}
                        players={Object.keys(props.gameState.players)}
                        thirdPerson={false}
                        playerName={currentPlayer}
                    />
                    <TimeoutDisplay gameState={props.gameState} />
                    <input type="submit" id="skipQuestionButton" onClick={Logic.nextQuestion} value={l.skipQuestion + ">>"}></input>
                    <PlayerDisplay players={players} size="small" />
                </span>
            );
        } else {
            // init skippbable true in case of unforeseen bugs
            let skippable = true;
            let timeoutSeconds = -1;
            if (props.gameState.whoHere.currentPlayer) {
                const player = props.gameState.players[props.gameState.whoHere.currentPlayer];
                timeoutSeconds = Math.round((new Date().getTime() - player.timestamp) / 1000);
                //skippable = timeoutSeconds > 10;
            }

            return <span><WaitingForCorrectAnswerScreen
                gameState={props.gameState}
                player={props.gameState.whoHere.currentPlayer!}
                skippable={skippable} timeoutSeconds={timeoutSeconds} onSkip={() => Logic.nextUser(true)} />
                <PlayerDisplay players={players} size="small" />
            </span>
        }

    } else if (groupAnswer == null) {
        // Group is deciding
        if (props.gameState.whoHere.currentPlayer !== Logic.getPlayer()) {
            var opinioneer = props.gameState.whoHere.currentPlayer


            return <span>
                <AnswerQuestionScreen
                    opinionHint={l.inPlayersOpinion(opinioneer)}
                    onSubmit={Logic.sendGroupAnswer}
                    question={question}
                    players={Object.keys(props.gameState.players)}
                    thirdPerson={true}
                    playerName={currentPlayer}
                />
                <PlayerDisplay players={players} size="small" 
                    useConsiderations={question.type === Logic.QuestionType.WHO_HERE}
                    considerations={props.gameState.whoHere.questionState.groupConsiderations}
                    considerationChanged={(player, consideration) => Logic.updateConsideration(player, consideration)}/>
            </span>
        } else {
            return <span>
                <WaitingForGroupAnswerScreen
                    question={question.question}
                    ownAnswer={correctAnswer.toString()} />
                <PlayerDisplay players={players} size="small"
                    considerations={props.gameState.whoHere.questionState.groupConsiderations}/>
            </span>
        }
    } else {
        // finished Question => show results
        return <span>
            <QuestionResultScreen onSubmit={() => Logic.nextUser(false)}
                player={props.gameState.whoHere.currentPlayer!}
                playerIsSelf={props.gameState.whoHere.currentPlayer === Logic.getPlayer()}
                question={question}
                correctAnswer={correctAnswer.toString()}
                groupAnswer={groupAnswer.toString()}
            />
            <PlayerDisplay players={players} size="small" />
        </span>
    }
};

function TimeoutDisplay(props: { gameState: GameState }) {
    React.useRef()
    var [, setIndex] = React.useState(0)
    React.useEffect(() => {
        var _index = 0;
        var intervalHandle = setInterval(() => setIndex(_index++), 500)
        return () => clearInterval(intervalHandle)
    }, [])
    var timeout = props.gameState.whoHere.questionState.startedTimeStamp - new Date().getTime();
    timeout = Math.round(timeout / 1000);
    timeout += props.gameState.whoHere.settings.timeForCorrectAnswer;

    return <span className="timeoutDisplay">{timeout}</span>
}
