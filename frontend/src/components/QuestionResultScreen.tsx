import * as React from 'react';
import { QuestionDisplay } from './QuestionDisplay';
import { play } from './audio';
import { useState } from 'react';
import { l } from '../localizations/localize';
import { selectQuestionText, Question } from '../gamelogic';

export function QuestionResultScreen(props: {
    onSubmit: () => void;
    hideSubmitButton?: boolean,
    question: Question;
    playerIsSelf: boolean;
    player: string
    correctAnswer: string;
    groupAnswer: string
}) {
    const [oldQuestion, setOldQuestion] = useState<Question>();
    const newQuestion = props.question;

    if (oldQuestion !== newQuestion) {
        setOldQuestion(props.question);
        if (props.correctAnswer === props.groupAnswer) {
            play("RightAnswer.wav")
        } else {
            play("WrongAnswer.wav")
        }
    }

    const groupDecisionHint = props.playerIsSelf ?
        l.groupThinksCurrentPlayer :
        l.groupThinksOtherPlayer;
    const playerAnswerHint = props.playerIsSelf ?
        l.currentPlayerThinks :
        l.playerThinks(props.player);
    const questionText = selectQuestionText(props.question, !props.playerIsSelf, props.player)

    return (
        <span>
            <QuestionDisplay hint={l.questionWas} question={questionText} />
            <div>
                {playerAnswerHint + " "}
                <span className="result-answer">{props.correctAnswer}</span>
            </div>
            <div>{groupDecisionHint + " "}<span className="result-answer">{props.groupAnswer}</span></div>
            { !props.hideSubmitButton ? <input type="submit" value={l.next} onClick={props.onSubmit} /> : undefined }
        </span>
    );
}
