import * as React from 'react';
import { LinkToThisRoom } from './LinkToThisRoom';
import { l } from '../localizations/localize';

export function AboutDisplay(props: { onClick: () => void }) {
    return (
        <span className="dataProtDisplay">
            <a href="#" onClick={() => {props.onClick(); return false}}>{l.about}</a>
        </span>)
}
