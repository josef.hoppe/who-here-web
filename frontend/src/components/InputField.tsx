import * as React from 'react';

export function InputField(props: { onSubmit: (value: string) => void, className?: string, submitButtonText: string, suggestions?: string[] }) {
    const [value, setValue] = React.useState<string>()
    const onSubmit = () => {
        if (value !== undefined)
            props.onSubmit(value)
    }
    const dataList = props.suggestions ? props.suggestions.map(sug => <option value={sug} />) : [];

    return <span className={props.className} >
        <input type="text" onChange={e => setValue(e.target.value)} onKeyPress={e => e.key === 'Enter' ? onSubmit() : undefined} list="suggestions" />
        <datalist id="suggestions">
            {dataList}
        </datalist>
        <input type="submit" value={props.submitButtonText} onClick={onSubmit} />
    </span>
}
