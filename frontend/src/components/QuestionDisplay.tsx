import * as React from 'react';

export function QuestionDisplay(props: {
    hint: string;
    question: string;
}) {
    return <span>
        <span className="opinionHint">{props.hint}</span>
        <span className="question-container">
            <span className="question" key="question">{props.question}</span>
        </span>
    </span>;
}
