import * as React from 'react';

export function LinkToThisRoom(room: string) {
    const url = new URL(document.URL)
    url.search = ""
    url.searchParams.set("room", room)
    const link = url.toString()
    return (link)
}
