import { useEffect } from 'react';
export function useSyncedUrlField(fieldName: string, onSubmit: (value: string) => void) {
    useEffect(() => {
        const url = new URL(document.URL);
        var paramValue = url.searchParams.get(fieldName);
        if (paramValue !== null) {
            onSubmit(paramValue);
        }
    }, []);
    return (value: string) => {
        const url = new URL(document.URL);
        url.searchParams.set(fieldName, value);
        const urlString = url.toString();
        history.pushState({ path: urlString }, document.title, urlString);
    };
}
