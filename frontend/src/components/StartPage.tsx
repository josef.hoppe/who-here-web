import * as React from 'react';
import { useState } from 'react';
import { createSession } from '../rooms';
import { InputField } from './InputField';
import { useSyncedUrlField } from "./useSyncedUrlField";
import * as Logic from '../gamelogic';
import { l } from '../localizations/localize';
import * as Locale from '../localizations/localize';

export const StartPage = (props: {
    onSubmit: (room: string) => void;
}) => {
    const [lang, setLang] = useState<"en" | "de">("de");

    const setLanguage: ((l: "en" | "de") => void) = l => {
        setLang(l);
        Locale.setLanguage(l);
    }

    const connectToExistingRoom = async (room: string) => {
        writeToUrl(room);
        await Logic.Connection.connectSessionWebsocket(room);
        props.onSubmit(room);
    };
    const writeToUrl = useSyncedUrlField("room", connectToExistingRoom);
    const connectToNewRoom = async () => {
        var newRoom = await createSession();
        await Logic.Connection.connectSessionWebsocket(newRoom);
        await Logic.initGameState();
        writeToUrl(newRoom);
        props.onSubmit(newRoom);
    };
    return <span>
        <span className="language-selection-startpage"><input className="language-button" type="submit" value="Deutsch" onClick={() => setLanguage('de')} /> &nbsp;
        <input type="submit" className="language-button" value="English" onClick={() => setLanguage('en')} /></span>
        <span className="title_lvl1">{l.title}</span>
        <div className="info">{l.gameDescription1}</div>
        <div className="info">{l.gameDescription2}</div>
        <div className="mark">{l.howItWorks}</div>
        <div className="explaination">{l.explanation}</div>
        <input type="submit" className="dark-button" value={l.createNewRoom} onClick={connectToNewRoom} />
    </span>;
};
