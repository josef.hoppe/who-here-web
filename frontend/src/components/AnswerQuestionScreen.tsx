import * as React from 'react';
import { useAudioEffectOnShow } from './audio';
import { InputField } from './InputField';
import { QuestionDisplay } from './QuestionDisplay';
import { l } from '../localizations/localize';
import { Answer, Consideration, MapObject, Question, QuestionType, ScaleQuestion, SelectQuestion, selectQuestionText } from '../gamelogic';

export function AnswerQuestionScreen(props: {
    opinionHint: string,
    onSubmit: (answer: Answer) => void,
    question: Question,
    playerName: string,
    thirdPerson: boolean,
    players: string[]
}) {
    useAudioEffectOnShow('QuestionReveal.wav')

    return (
        <span>
            <QuestionDisplay hint={props.opinionHint} question={selectQuestionText(props.question, props.thirdPerson, props.playerName)} />
            <AnswerComponent onSubmit={props.onSubmit} question={props.question} players={props.players} />
        </span>
    );
};

function AnswerComponent(props: {
    onSubmit: (answer: Answer) => void,
    players: string[]
    question: Question
}) {
    const question = props.question
    if (question.type === QuestionType.SCALE)
        return <Slider onSubmit={props.onSubmit} question={question} />
    if (question.type === QuestionType.SELECT)
        return <Choice onSubmit={props.onSubmit} question={question} />
    else
        return <InputField 
            onSubmit={props.onSubmit} 
            submitButtonText={l.submit} 
            suggestions={
                question.type === QuestionType.WHO_HERE ? 
                    props.players : 
                    undefined}
            />
}

function Slider(props: {
    onSubmit: (answer: number) => void,
    question: ScaleQuestion
}) {
    const question = props.question
    const [value, setValue] = React.useState(Math.floor((question.min + question.max) / 2))
    return <span>
        <div>{value}</div>
        <input type="range" min={question.min} max={question.max} value={value} onChange={e => setValue(parseInt(e.target.value))} />
        <input type="submit" value={l.submit} onClick={() => props.onSubmit(value)} />
    </span>
}

function Choice(props: {
    onSubmit: (answer: string) => void,
    question: SelectQuestion
}) {
    const question = props.question
    return <span>
        {
            question.choices.map(choice => <input type="submit" value={choice} onClick={() => props.onSubmit(choice)}/>)
        }
    </span>
}