import * as React from 'react';
import { useAudioEffectOnShow } from './audio';
import { QuestionDisplay } from './QuestionDisplay';
import { l } from '../localizations/localize';
import { QuestionResultScreen } from './QuestionResultScreen';
import { GameState, getPlayer } from '../gamelogic';

export function WaitingForCorrectAnswerScreen(props: {
    player: string,
    skippable: boolean,
    timeoutSeconds: number,
    gameState: GameState,
    onSkip: () => void
}) {
    useAudioEffectOnShow("StateChangeOther.wav")
    const spaces = Array(5).join("\u2000 ");

    const lastQuestionState = props.gameState.whoHere.lastQuestionState || {
        questionIndex: 0,
        correctAnswer: "",
        groupAnswer: ""
    }

    const oldQuestionDisplay = props.gameState.whoHere.lastQuestionState == null ? undefined : 
        <QuestionResultScreen onSubmit={() => console.log("")}
            hideSubmitButton={true}
            player={props.gameState.whoHere.lastPlayer!}
            playerIsSelf={props.gameState.whoHere.lastPlayer === getPlayer()}
            question={props.gameState.whoHere.questions[lastQuestionState.questionIndex]} 
            correctAnswer={lastQuestionState.correctAnswer?.toString() || ""} 
            groupAnswer={lastQuestionState.groupAnswer?.toString() || ""} 
        />

    return (
        <span>
            {oldQuestionDisplay}
            <QuestionDisplay hint={l.inPlayersOpinion(props.player)} question={spaces + "???" + spaces} />
            <div>{l.waitingForPlayerAnswer(props.player)}</div>
            {props.skippable ? <div>
                {props.timeoutSeconds > 10 ? l.playerInactive(props.timeoutSeconds) : undefined}
                <input type="submit" onClick={props.onSkip} value={l.skipPlayer}></input>
            </div> : undefined}
        </span>
    );
};