import * as React from 'react';

export function useAudioEffectOnShow(path: string) {
    React.useEffect(() => play(path), []);
}
export function play(path: string) {
    var audio = new Audio(require('../../sounds/' + path).default);
    audio.play();
}

