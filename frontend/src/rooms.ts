import { backendUrl } from "./_backendUrl"

export async function createSession() {
    const result = await fetch(backendUrl + "api/v1/addSession", { method: "POST" })
    const sessionString = await result.text()
    return sessionString
}
