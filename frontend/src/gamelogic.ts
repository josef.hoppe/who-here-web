import { backendUrl } from './_backendUrl'
import { ServerConnection, RecursivePartial } from './connection'
import { getLanguage } from './localizations/localize';


interface LogicState {
    player: string | null,
    questions: Question[],
    categoryTranslations: {
        [key: string]: {
            [key: string]: string
        }
    }
}

const logicState: LogicState = {
    player: null,
    questions: [],
    categoryTranslations: {}
}

export interface Player {
    name: string,
    timestamp: number
}

export interface GameState {
    players: MapObject<Player>,
    game: string,
    whoHere: WhoHereState
}

export interface WhoHereState {
    currentPlayer: string | null,
    questionState: QuestionState,
    lastQuestionState: QuestionState | null,
    lastPlayer: string | null,
    stage: "lobby" | "game",
    questions: Question[],
    settings: {
        lang: string,
        categories: MapObject<boolean>,
        types: MapObject<boolean>,
        timeForCorrectAnswer: number
    }
}

export enum Consideration {
    NONE = "consNone",
    POSITIVE = "consPos",
    NEGATIVE = "consNeg"
}

export interface QuestionState {
    questionIndex: number,
    startedTimeStamp: number,
    correctAnswer: Answer | null,
    groupAnswer: Answer | null,
    groupConsiderations: MapObject<Consideration>
}

interface QuestionBase {
    category: string,
    language: string,
    question: string,
    questionThirdPerson?: string
}

export enum QuestionType {
    WHO_HERE = 'whohere',
    SCALE = 'scale',
    SELECT = 'select',
    TEXT = 'text'
}

export interface WhoHereQuestion extends QuestionBase {
    type: QuestionType.WHO_HERE
}

export interface ScaleQuestion extends QuestionBase {
    type: QuestionType.SCALE
    min: number
    max: number
}

export interface SelectQuestion extends QuestionBase {
    type: QuestionType.SELECT
    choices: string[]
}

export interface TextQuestion extends QuestionBase {
    type: QuestionType.TEXT
}

export type Question =
    WhoHereQuestion
    | ScaleQuestion
    | SelectQuestion
    | TextQuestion


export interface MapObject<T> {
    [key: string]: T
}

function wrapInMapObject<T>(key: string, value: T): MapObject<T> {
    const result: MapObject<T> = {}
    result[key] = value
    return result
}

export type Answer = string | number;

export const Connection = new ServerConnection<GameState, {}>();

function extractObject<T, S>(arr: T[], keyExtractor: (elem: T) => string, valueFunction: (elem: T) => S): MapObject<S> {
    const res: MapObject<S> = {};
    arr.forEach(element => {
        const key = keyExtractor(element);
        if (!res.hasOwnProperty(key)) {
            res[key] = valueFunction(element);
        }
    });

    return res;
}

export function startHeartbeat() {
    setInterval(sendHeartbeat, 5000);
}

function sendHeartbeat() {
    if (logicState.player !== null) {
        const players: MapObject<any> = {};
        const player = {
            timestamp: new Date().getTime()
        }
        players[logicState.player] = player;
        Connection.sendPartialUpdate({
            players: players
        });
    }
}

async function fetchQuestions() {
    if (logicState.questions.length == 0) {
        const res = await fetch(backendUrl + 'api/v1/data');
        const dataString = await res.text();
        const data = JSON.parse(dataString);
        logicState.categoryTranslations = data.categoryTranslations;
        logicState.questions = data.data;

        if (!getState()?.whoHere?.settings) {
            const types = extractObject<Question, boolean>(data.data, q => q.type, () => true);
            const categories = extractObject<Question, boolean>(data.data, q => q.category, () => true);
            Connection.sendPartialUpdate({
                whoHere: {
                    settings: {
                        lang: getLanguage(),
                        types: types,
                        categories: categories,
                        timeForCorrectAnswer: 40
                    }
                }
            })
        }
    }
}

export async function initGameState() {
    Connection.sendPartialUpdate({
        players: {},
        game: "whohere",
        whoHere: {
            currentPlayer: null,
            questionState: {
                questionIndex: 0,
                startedTimeStamp: 0,
                correctAnswer: null,
                groupAnswer: null
            },
            stage: "lobby"
        }
    });

    fetchQuestions();
}

function buildNextQuestion(): QuestionState {
    const state = getState();

    return {
        questionIndex: (state.whoHere.questionState.questionIndex + 1) % state.whoHere.questions.length,
        startedTimeStamp: new Date().getTime(),
        correctAnswer: null,
        groupAnswer: null,
        groupConsiderations: {}
    }
}

// ends the lobby stage
export async function startGame() {
    const firstPlayer = Object.keys(getState().players)[0];
    const settings = getState().whoHere.settings;

    const questions = logicState.questions
        .filter(q => settings.categories[q.category])
        .filter(q => settings.types[q.type])
        .filter(q => q.language === settings.lang);

    shuffle(questions);

    await Connection.sendPartialUpdate({
        whoHere: {
            currentPlayer: firstPlayer,
            stage: "game",
            questions: questions
        }
    });
}

export async function nextQuestion() {

    await Connection.sendPartialUpdate({
        whoHere: {
            questionState: buildNextQuestion()
        }
    });
}

export async function nextUser(skip = false) {
    const state = getState();
    const users = Object.keys(state.players);
    const curUserIndex = state.whoHere.currentPlayer ? users.indexOf(state.whoHere.currentPlayer) : -1;
    const nextUserIndex = (curUserIndex + 1) % users.length;
    const next = users[nextUserIndex];

    await Connection.sendPartialUpdate({
        whoHere: {
            currentPlayer: next,
            questionState: buildNextQuestion(),
            ...(skip ? undefined : {
                lastQuestionState: state.whoHere.questionState,
                lastPlayer: state.whoHere.currentPlayer
            })
        }
    })
}

export async function sendCorrectAnswer(answer: Answer) {
    await Connection.sendPartialUpdate({
        whoHere: {
            questionState: {
                correctAnswer: answer
            }
        }
    })
}

export async function updateConsideration(player: string, consideration: Consideration) {
    await Connection.sendPartialUpdate({
        whoHere: {
            questionState: {
                groupConsiderations: wrapInMapObject(player, consideration)
            }
        }
    })
}

export async function sendGroupAnswer(answer: Answer) {
    await Connection.sendPartialUpdate({
        whoHere: {
            questionState: {
                groupAnswer: answer
            }
        }
    })
}

export async function setPlayer(player: string) {
    fetchQuestions();
    logicState.player = player;
    await Connection.sendPartialUpdate({ players: wrapInMapObject(player, { name: player }) })
}

export function getPlayer() {
    return logicState.player;
}

export function getState(): GameState {
    return Connection.getState();
}

export function setTypeSetting(key: string, value: boolean) {
    const typeSettings: MapObject<boolean> = {};
    typeSettings[key] = value;

    Connection.sendPartialUpdate({
        whoHere: {
            settings: {
                types: typeSettings
            }
        }
    })
}

export function setCategorySetting(key: string, value: boolean) {
    const settings: MapObject<boolean> = {};
    settings[key] = value;

    Connection.sendPartialUpdate({
        whoHere: {
            settings: {
                categories: settings
            }
        }
    })
}

export function setLang(lang: string) {
    Connection.sendPartialUpdate({
        whoHere: {
            settings: {
                lang: lang
            }
        }
    })
}

export function setTimeForCorrectAnswer(seconds: number) {
    Connection.sendPartialUpdate({
        whoHere: {
            settings: {
                timeForCorrectAnswer: seconds
            }
        }
    })
}

export function localizeCategory(category: string, lang: string) {
    const langTranslations = logicState.categoryTranslations[lang];
    if (!langTranslations) return category;
    const translation = langTranslations[category];
    return translation ? translation : category;
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a: any[]) {
    if (!a) return;

    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}
export function selectQuestionText(question: Question, thirdPerson: boolean, playerName: string) {
    var questionString = (thirdPerson ? question.questionThirdPerson : undefined) ?? question.question;
    questionString = questionString.replace(/{playerName}/g, playerName);
    return questionString;
}
