import { wsBackendUrl } from "./_backendUrl"

type SocketMessage<T extends object> = IncrementalUpdateMessage<T> | StateUpdateMessage<T> | EventMessage;

interface IncrementalUpdateMessage<T extends object> {
    type: 'INCREMENTAL_UPDATE',
    payload: RecursivePartial<T>
}

interface StateUpdateMessage<T extends object> {
    type: 'STATE_UPDATE',
    payload: T
}

interface EventMessage {
    type: 'EVENT',
    payload: any
}
export type UpdateHandler<T> = ((newState: T) => void)
export type EventHandler<T> = ((event: T) => void)

export type RecursivePartial<T> = {
    [P in keyof T]?: RecursivePartial<T[P]>;
};

export class ServerConnection<TState extends object, TEvent extends object> {
    private appState: TState
    private websocket: WebSocket | null
    private updateHandlers: UpdateHandler<TState>[]
    private eventHandlers: EventHandler<TEvent>[]
    private sessionId: string

    constructor() {
        this.appState = <any>{};
        this.websocket = null;
        this.updateHandlers = [];
        this.eventHandlers = [];
        this.sessionId = "";
    }

    private static applyIncrementalUpdate<TParam extends object>(oldState: TParam, update: RecursivePartial<TParam>): TParam {
        if (update === null) return oldState

        const newState = { ...oldState }
        for (const key in update) {
            if (update.hasOwnProperty(key)) {
                const element = update[key];

                if (typeof element === 'object' && oldState.hasOwnProperty(key) && typeof oldState[key] === 'object') {
                    // continue deep update
                    newState[key] = this.applyIncrementalUpdate(<any>oldState[key], <any>element);
                } else {
                    // property is not an object or doesn't exist, just replace it
                    newState[key] = <any>element;
                }
            }
        }
        return newState;
    }

    private handleMessage(event: MessageEvent) {
        const msg: SocketMessage<TState> = JSON.parse(event.data);
        switch (msg.type) {
            case 'INCREMENTAL_UPDATE':
                this.appState = ServerConnection.applyIncrementalUpdate(this.appState, msg.payload);
                this.notifyObservers();
                break;
            case 'STATE_UPDATE':
                this.appState = msg.payload;
                this.notifyObservers();
                break;
            case 'EVENT':
                this.eventHandlers.forEach(handler => handler(msg.payload))
        }
    }

    private notifyObservers() {
        this.updateHandlers.forEach((handler) => {
            handler(this.appState);
        })
    }

    public async sendPartialUpdate(update: RecursivePartial<TState>) {
        const msg: IncrementalUpdateMessage<TState> = { type: 'INCREMENTAL_UPDATE', payload: update }
        this.websocket?.send(JSON.stringify(msg));
    }

    // do not use directly, use the getState method of the correct game logic instead.
    public getState() {
        return this.appState;
    }

    public addUpdateHandler(handler: UpdateHandler<TState>) {
        this.updateHandlers.push(handler);
    }

    public publishEvent(event: any) {
        const msg: SocketMessage<TState> = {
            type: 'EVENT',
            payload: event
        }
        this.websocket?.send(JSON.stringify(msg));
    }

    public addEventHandler(handler: EventHandler<TEvent>) {
        this.eventHandlers.push(handler);
    }

    public async connectSessionWebsocket(session: string) {
        if (this.sessionId !== session) {
            this.sessionId = session;
            if (this.websocket !== null) {
                this.websocket.close();
            }
            await this.openWebsocket(session);
        }
    }

    private openWebsocket(session: string) {
        return new Promise((resolve, reject) => {
            const url = wsBackendUrl + '/api/v1/connect?session=' + session;
            this.websocket = new WebSocket(url);

            var isOpen = false

            this.websocket.onopen = () => {
                this.websocket?.send('{"type":"STATE_REQUEST"}');
                isOpen = true
                resolve();
            }
            this.websocket.onmessage = e => this.handleMessage(e);
            this.websocket.onclose = () => {
                if (this.sessionId === session) {
                    this.openWebsocket(session);
                }
                if (!isOpen) {
                    reject();
                }
            }
            this.websocket.onerror = () => {
                if (!isOpen)
                    reject();
            };
        });
    }
}