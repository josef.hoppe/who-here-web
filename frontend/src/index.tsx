import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './components/App';

require('./style.css')

const favicon = require("../img/favicon.ico").default
document.head.innerHTML += `
    <link rel="shortcut icon" href="/${favicon}" type="image/x-icon">
    <link rel="icon" href="/${favicon}" type="image/x-icon"></link>
    `

const uiTarget = document.createElement("div")
uiTarget.id = "WhoHere"
document.body.append(uiTarget)
ReactDOM.render(
<App />, uiTarget);
