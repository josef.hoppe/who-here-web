import { en } from "./en"
import { de } from "./de"

var currentLanguage = "de"
export function setLanguage(language: "en" | "de") {
    currentLanguage = language
    selectLocalizations();
}
function selectLocalizations() {
    switch (currentLanguage) {
        case "de":
            l = de;
            break;
        case "en":
            l = en;
            break;
        default:
            break;
    }
}

export function getLanguage() {
    return currentLanguage
}

export function getAvailableLangs() {
    return ["en", "de"]
}

export var l = en
selectLocalizations();

