import { LocalizedStrings } from "./LocalizedStrings";

export const en: LocalizedStrings = {
    title: "Who here?",
    gameDescription1: "What do my friends think about me? What do I think about them?",
    gameDescription2: "Welcome to WhoHere, a game which solves these questions and many more.",
    createNewRoom: "Create new room.",
    howItWorks: "How it works.",
    inPlayersOpinion: opinioneer => {
        if (opinioneer === null) {
            opinioneer = "others";
        } else {
            const lastChar = opinioneer[opinioneer.length - 1];
            if (lastChar === "x" || lastChar === "s") {
                opinioneer = opinioneer + "'";
            } else {
                opinioneer = opinioneer + "'s";
            }
        }
        return `In ${opinioneer} opinion:`
    },
    inYourOpinion: "In your opinion",
    submit: "Submit",
    copyIconAltText: "Copy Icon",
    enterNameToJoin: "Enter your name to join the room:",
    playerListTitle: "Players:",
    linkToRoom: "Link to this room:",
    settingsTitle: "Settings:",
    settingsQuestionTypeTitle: "Question types:",
    settingsQuestionCategoryTitle: "Question categories:",
    settingsMiscTitle: "Miscellaneous:",
    start: "start",
    playerThinks: player => `${player} thinks it's`,
    next: "next >>",
    currentPlayerThinks: "You think",
    questionWas: "The question was:",
    groupThinksCurrentPlayer: "The rest thought you chose",
    groupThinksOtherPlayer: "The rest thought they chose",
    skipPlayer: "Skip Player",
    skipQuestion: "Skip Question",
    waitingForPlayerAnswer: player => `Waiting for ${player} to answer...`,
    playerInactive: seconds => `Player inactive for ${seconds} s.`,
    waitingForGroupAnswer: "Waiting for group...",
    join: "Join",
    explanation: "We recommend using a voice chat (e.g. Discord or Skype) parallel to the game. "
        + "Your task is to assess your fellow players as accurately as possible. "
        + "Each round one player answers a hidden question. "
        + "Afterwards, the remaining players consult each other in the voice chat and try to guess how the active player might have answered.",
    questionTypes: {
        whohere: 'Who here... ?',
        scale: 'On a scale... ?',
        select: 'Selection',
        text: 'Free text'
    },
    about: 'About',
    dataProtection: 'Who Here doesn\'t track the users. '
        + 'However, some information is temporarily stored.',
    license: 'This program is free software, licensed under the GNU AGPLv3.',
    moreInfo: 'More information',
    back: 'Back',
    timeForCorrectAnswerSetting: 'Answertime (non-group phase)',
    languageSetting: 'Language',
    timeRemaining: 'Time remaining',
    noCats: 'You have to choose at least one category',
    noTypes: 'You have to choose at least one question type'
}