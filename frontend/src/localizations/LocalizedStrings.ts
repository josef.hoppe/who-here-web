import { QuestionType } from "../gamelogic";

export interface LocalizedStrings {
    timeRemaining: string;
    settingsMiscTitle: string;
    timeForCorrectAnswerSetting: string;
    languageSetting: string;
    title: string,
    gameDescription1: string,
    gameDescription2: string,
    createNewRoom: string,
    howItWorks: string,
    inPlayersOpinion: (playerName: string | null) => string,
    inYourOpinion: string
    submit: string,
    copyIconAltText: string,
    enterNameToJoin: string,
    playerListTitle: string,
    linkToRoom: string,
    settingsTitle: string,
    settingsQuestionTypeTitle: string,
    settingsQuestionCategoryTitle: string,
    next: string
    start: string
    playerThinks: (player: string) => string,
    currentPlayerThinks: string,
    questionWas: string,
    groupThinksCurrentPlayer: string,
    groupThinksOtherPlayer: string,
    skipPlayer: string,
    skipQuestion: string,
    waitingForPlayerAnswer: (player: string) => string,
    playerInactive: (seconds: number) => string,
    waitingForGroupAnswer: string,
    join: string,
    explanation: string
    questionTypes: {
        [key in QuestionType]: string
    },
    about: string
    dataProtection: string
    license: string
    moreInfo: string
    back: string
    noCats: string
    noTypes: string
}
