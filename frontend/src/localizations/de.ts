import { LocalizedStrings } from "./LocalizedStrings";

export const de: LocalizedStrings = {
    title: "Who here?",
    gameDescription1: "Was denken meine Freunde über mich? Was denke ich über sie?",
    gameDescription2: "Willkommen bei WhoHere, einem Spiel, das diese und viele weitere Fragen löst.",
    createNewRoom: "Neuen Raum erstellen.",
    howItWorks: "So funktioniert's.",
    inPlayersOpinion: opinioneer => {
        if (opinioneer === null) {
            opinioneer = "Irgendwem";
        } else if (opinioneer[opinioneer.length - 1] !== "s" && opinioneer[opinioneer.length - 1] !== "x") {
            opinioneer = opinioneer + "s";
        } else {
        }
        return `${opinioneer} Meinung nach:`
    },
    inYourOpinion: "Deiner Meinung nach:",
    submit: "Absenden",
    copyIconAltText: "Kopiersymbol",
    enterNameToJoin: "Zum Beitreten Namen eingeben:",
    playerListTitle: "Mitspieler:",
    linkToRoom: "Link um beizutreten:",
    settingsTitle: "Einstellungen:",
    settingsQuestionTypeTitle: "Fragenarten:",
    settingsQuestionCategoryTitle: "Fragenkategorien:",
    settingsMiscTitle: "Allgemein:",
    start: "Los geht's!",
    playerThinks: player => `${player} meint `,
    currentPlayerThinks: "Du meinst ",
    questionWas: "Die Frage lautete:",
    next:"weiter >>",
    groupThinksCurrentPlayer: "Die anderen dachten, du meintest ",
    groupThinksOtherPlayer: "Ihr dachtet, die Antwort sei ",
    skipPlayer: "Spieler überspringen",
    skipQuestion: "Frage überspringen",
    waitingForPlayerAnswer: player => `${player} beantwortet die Frage...`,
    playerInactive: seconds => `Spieler seit ${seconds}s inaktiv`,
    waitingForGroupAnswer: 'Die Gruppe beantwortet die Frage...',
    explanation: "Wir empfehlen parallel zu dem Spiel einen VoiceChat zu verwenden (z.B. Discord oder Skype). Eure Aufgabe ist es, eure Mitspieler möglichst genau einzuschätzen. Jede Runde beantwortet ein Spieler eine verdeckte Frage. Anschließend beraten sich die restlichen Spieler im Voicechat und versuchen zu erraten, wie der aktive Spieler geantwortet haben könnte.",
    join: 'Beitreten',
    questionTypes: {
        whohere: 'Wer hier... ?',
        scale: 'Auf einer Skala... ?',
        select: 'Auswahl',
        text: 'Freitext'
    },
    about: 'Über',
    dataProtection: 'Der Benutzer wird nicht von Who Here getrackt, es werden insbesondere auch keine IP-Adressen aufgezeichnet. Einige Daten werden temporär vorgehalten.',
    license: 'Dieses Programm ist freie Software, lizensiert mit der GNU AGPLv3.',
    moreInfo: 'Mehr Informationen',
    back: 'Zurück',
    timeForCorrectAnswerSetting: 'Antwortzeit (nicht Gruppe)',
    languageSetting: 'Sprache',
    timeRemaining: 'verbleibende Zeit',
    noCats: 'Du musst mindestens eine Kategorie auswählen',
    noTypes: 'Du musst mindestens einen Fragentyp auswählen'
}