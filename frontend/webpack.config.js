const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

var mode = process.env.NODE_ENV // 'development'

module.exports = (env, argv) => {
    var mode = argv.mode
    var config = {
        entry: {
            app: './src/index.tsx'
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist')
        },
        devtool: 'inline-source-map',
        devServer: {
            contentBase: './dist'
        },
        mode: mode,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: mode === 'development',
                            experimentalWatchApi: mode === 'development',
                        },
                    },
                    exclude: /node_modules/,
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                hmr: mode === 'development',
                            }
                        },
                        'css-loader'
                    ]
                },
                {
                    test: /\.(wav|mp3|ico)$/,
                    use: 'file-loader'
                }
            ]
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        plugins: [
            new HtmlWebpackPlugin({ title: `Who here...?${mode !== 'production'? `  ${mode} build)`:''}` }),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css'
            })
        ],
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
        devServer: {
            headers: {
                // "Access-Control-Allow-Origin": "http://localhost:8000/" 
                "Access-Control-Allow-Origin": "*"
            }
        }
    }
    return config;
};
