package io.hoppe.whohere

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.io.File

@RestController
@CrossOrigin
class DataController {

    private final val mapper = ObjectMapper()
    private lateinit var jsonData: ObjectNode

    init {
        try {
            loadData()
        } catch (_: java.lang.Exception) {
            jsonData = mapper.createObjectNode()
            jsonData.put("status", "error")
            jsonData.put("message", "failed to load data")
        }
    }

    @PostMapping("/api/v1/loadData")
    private fun loadData() {
        try {
            val dataFile = File("data.json")
            jsonData = mapper.readValue(dataFile, ObjectNode::class.java)
        } catch (e: Exception) {
            println("error loading data file ${e.localizedMessage}")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, e.localizedMessage)
        }
    }

    @GetMapping("/api/v1/data")
    fun getData(): ObjectNode {
        return jsonData
    }
}