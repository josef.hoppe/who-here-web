# JSON SESSION API

## Create a session

``
POST /api/v1/addSession
``

Result: Session ID (Uppercase character String, i.e. `TKJETAYV`)

## WebSocket

Connect to `/api/v1/connect?session=[session id]`

### Request the state

WS message: `{"type":"STATE_REQUEST"}`. The response is a Status update message (see below). There are no side effects.

### Incremental Update

An incremental update updates all properties in the update object, but leaves all other intact. This is applied deep (beter description needed)

Incremental updates are broadcasted to all clients of the same session.


WS message: `{"type":"INCREMENTAL_UPDATE", "payload": "[update object]"}`

**Example for state / update objects**

*state before*

```json
{
    "a" : {
        "foo": "bar",
        "xy": ["z"]
    }
}
```

*incremental update object*

```json
{
    "a" : {
        "boo": "far",
        "xy": 2
    },
    "b" : [1,2]
}
```

*resulting state*

```json
{
    "a" : {
        "foo": "bar",
        "boo": "far",
        "xy": 2
    },
    "b" : [1,2]
}
```

### Full State Update

A full status update replaces the current state with the given payload. Full status updates are broadcasted to all clients of the same session.

WS message: `{"type":"STATE_UPDATE", "payload": "[new status]"}`